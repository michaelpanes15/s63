/*
- Clone the given repository in your s63 folder.
- Rename the cloned folder as activity
- Execute command: npm i inside the active folder to install the dependencies.
- Try npm test command to run the test.
- Create your function coding solution in index.js
- DO NOT refactor the test.js file.
- DO "npm test" every time there is a change in the code that needed to be tested.
- Create a new personal repo folder named s63 and create a repo project named activity inside and push your mock-tech exam

*/

function countLetter(letter, sentence) {
    let result = 0;
    if(letter.length > 1) return undefined;
    else {
        let count = 0;
        for(let i=0; i<sentence.length-1; i++){
            if (sentence[i] === letter) count++;
        }
        return count;
    }

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    let newText = text.toLowerCase();
    const textMap = {};
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    for(let i=0; i<newText.length-1; i++){
      if (textMap[newText[i]]) {
        return false; 
      } else {
        textMap[newText[i]] = true;
      }
    }
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if(age < 13) return undefined;
    else if ((age >= 13 && age <= 21) || age > 64) {
        return (Math.round((price*0.8)*100)/100).toString();
    }
    else return (Math.round((price*100))/100).toString();
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let hotCategories = [];
    let filteredItems = items.filter(item => item.stocks === 0).map(item => item.category);
    filteredItems.forEach(item => {
        if(!hotCategories.includes(item)) hotCategories.push(item);
    })
    return hotCategories;


}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    const candidateBSet = new Set(candidateB);
    let flyingVoters = [];
    candidateA.forEach(voter => {
        if(candidateBSet.has(voter)) flyingVoters.push(voter);
    });
    return flyingVoters;
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};